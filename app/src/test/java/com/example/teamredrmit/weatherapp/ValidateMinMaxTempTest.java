package com.example.teamredrmit.weatherapp;

import com.example.teamredrmit.weatherapp.Utils.Validator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by rudhrakumargurunathan on 28/05/16.
 * test the temperature conversion returns correct values for + ve & -ve Temperatures
 */

public class ValidateMinMaxTempTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testNegativeTempCeiling() {
        Double negativeCeilingVal = -15.51;
        assertEquals(Validator.round(negativeCeilingVal),-16.0,0.0001);
    }
    @Test
    public void testNegativeTempFloor() {
        Double negativeFloorVal = -1.29;
        assertEquals(Validator.round(negativeFloorVal),-1.0,0.0001);
    }
    @Test
    public void testPositiveTempCeiling() {
        Double positiveCeilingVal = 15.51;
        assertEquals(Validator.round(positiveCeilingVal),16.0,0.0001);
    }
    @Test
    public void testPositiveTempFloor() {
        Double positiveFloorVal = 1.29;
        assertEquals(Validator.round(positiveFloorVal),1.0,0.0001);
    }

    @Test
    public void testZeroTemp() {
        Double valZero = 0.0;
        assertEquals(Validator.round(valZero),0.0,0.0001);
    }
}
