package com.example.teamredrmit.weatherapp;

import com.example.teamredrmit.weatherapp.Model.ForecastData;

import org.junit.Test;
import static org.junit.Assert.*;

public class ForecastDataTest {

    //Following code tests all the getters and setters of the ForecastData class.
    ForecastData fd = new ForecastData();
    double a = 1.00;
    int b = 2;
    String c = "Hello";

    @Test
    public void offsetTest() {
        fd.setTime(b);
        assertTrue(b == fd.getTime());
    }

    @Test
    public void timezoneTest() {
        fd.setSummary(c);
        assertEquals(c, fd.getSummary());
    }

    @Test
    public void iconTest() {
        fd.setIcon(c);
        assertEquals(c, fd.getIcon());
    }

    @Test
    public void sunriseTimeTest() {
        fd.setSunriseTime(b);
        assertTrue(b == fd.getSunriseTime());
    }

    @Test
    public void sunsetTimeTest() {
        fd.setSunsetTime(b);
        assertTrue(b == fd.getSunsetTime());
    }

    @Test
    public void moonPhaseTest() {
        fd.setMoonPhase(a);
        assertTrue(a == fd.getMoonPhase());
    }

    @Test
    public void precipIntensityTest() {
        fd.setPrecipIntensity(a);
        assertTrue(a == fd.getPrecipIntensity());
    }

    @Test
    public void precipIntensityMaxTest() {
        fd.setPrecipIntensityMax(a);
        assertTrue(a == fd.getPrecipIntensityMax());
    }

    @Test
    public void precipIntensityMaxTimeTest() {
        fd.setPrecipIntensityMaxTime(b);
        assertTrue(b == fd.getPrecipIntensityMaxTime());
    }

    @Test
    public void precipProbabilityTest() {
        fd.setPrecipProbability(a);
        assertTrue(a == fd.getPrecipProbability());
    }

    @Test
    public void precipTypeTest() {
        fd.setPrecipType(c);
        assertEquals(c, fd.getPrecipType());
    }

    @Test
    public void tempMinTest() {
        fd.setTemperatureMin(a);
        assertTrue(a == fd.getTemperatureMin());
    }

    @Test
    public void tempMinTimeTest() {
        fd.setTemperatureMinTime(b);
        assertTrue(b == fd.getTemperatureMinTime());
    }

    @Test
    public void tempMaxTest() {
        fd.setTemperatureMax(a);
        assertTrue(a == fd.getTemperatureMax());
    }

    @Test
    public void tempMaxTimeTest() {
        fd.setTemperatureMaxTime(b);
        assertTrue(b == fd.getTemperatureMaxTime());
    }

    @Test
    public void appTempMinTest() {
        fd.setApparentTemperatureMin(a);
        assertTrue(a == fd.getApparentTemperatureMin());
    }

    @Test
    public void appTempMinTimeTest() {
        fd.setApparentTemperatureMinTime(b);
        assertTrue(b == fd.getApparentTemperatureMinTime());
    }

    @Test
    public void appTempMaxTest() {
        fd.setApparentTemperatureMax(a);
        assertTrue(a == fd.getApparentTemperatureMax());
    }

    @Test
    public void appTempMaxTimeTest() {
        fd.setApparentTemperatureMaxTime(b);
        assertTrue(b == fd.getApparentTemperatureMaxTime());
    }

    @Test
    public void dewPointTest() {
        fd.setDewPoint(a);
        assertTrue(a == fd.getDewPoint());
    }

    @Test
    public void humidityTest() {
        fd.setHumidity(a);
        assertTrue(a == fd.getHumidity());
    }

    @Test
    public void windSpeedTest() {
        fd.setWindSpeed(a);
        assertTrue(a == fd.getWindSpeed());
    }

    @Test
    public void windBearingTest() {
        fd.setWindBearing(b);
        assertTrue(b == fd.getWindBearing());
    }

    @Test
    public void cloudCoverTest() {
        fd.setCloudCover(a);
        assertTrue(a == fd.getCloudCover());
    }

    @Test
    public void pressureTest() {
        fd.setPressure(a);
        assertTrue(a == fd.getPressure());
    }

    @Test
    public void ozoneTest() {
        fd.setOzone(a);
        assertTrue(a == fd.getOzone());
    }
}
