package com.example.teamredrmit.weatherapp;

import com.example.teamredrmit.weatherapp.Utils.Validator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by rudhrakumargurunathan on 28/05/16.
 * test the temperature conversion returns correct values for + ve & -ve Temperatures
 */

public class ValidateSearchInputTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testEmptyInput() {
        String input = "";
        assertEquals(Validator.validateSearchInput(input),"isEmpty");
    }
    @Test
    public void testEmptyInput2() {
        String input = " ";
        assertEquals(Validator.validateSearchInput(input),"isEmpty");
    }
    @Test
    public void testForNumericInput() {
        String input = "12345";
        assertEquals(Validator.validateSearchInput(input),"InvalidInput");
    }
    @Test
    public void testForNonAlphabetInput() {
        String input = "!@#$";
        assertEquals(Validator.validateSearchInput(input),"InvalidInput");
    }
    @Test
    public void testForValidText() {
        String input = "Melbourne";
        assertEquals(Validator.validateSearchInput(input),"ValidInput");
    }

    @Test
    public void testForValidText2() {
        String input = "Melbourne Airport";
        assertEquals(Validator.validateSearchInput(input),"ValidInput");
    }

    @Test
    public void testForValidText3() {
        String input = " Melbourne Airport ";
        assertEquals(Validator.validateSearchInput(input),"ValidInput");
    }
}
