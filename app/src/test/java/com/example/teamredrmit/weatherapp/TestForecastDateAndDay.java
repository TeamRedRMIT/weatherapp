package com.example.teamredrmit.weatherapp;
import com.example.teamredrmit.weatherapp.Utils.Validator;

import org.junit.Test;

import static junit.framework.Assert.*;

/**
 * Created by rudhrakumargurunathan on 28/05/16.
 */
public class TestForecastDateAndDay {

    @Test
    public void testConvertTimeStampToDay(){
        int unixTimeStamp = 1464407015; // "Expected Local Day: Sat"
        assertEquals(Validator.getWeekDay(unixTimeStamp),"Sat");
    }

    @Test
    public void testConvertTimeStampToTime(){
        int unixTimeStamp = 1464412484; //"Expected Local Time 03:14"
        assertEquals(Validator.getTime(unixTimeStamp),"03:14" );
    }
}
