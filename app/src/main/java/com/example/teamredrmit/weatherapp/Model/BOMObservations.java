package com.example.teamredrmit.weatherapp.Model;

import java.util.List;

public class BOMObservations {
    private List<Observation> data;

    public List<Observation> getData() {
        return data;
    }

    public void setData(List<Observation> o) {
        data = o;
    }
}
