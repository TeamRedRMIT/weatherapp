package com.example.teamredrmit.weatherapp.Model;

import com.example.teamredrmit.weatherapp.Model.Observation;
import com.example.teamredrmit.weatherapp.Model.Station;

public interface ObservationListener {
    void onObservationAdded(Observation o, Station s);
}