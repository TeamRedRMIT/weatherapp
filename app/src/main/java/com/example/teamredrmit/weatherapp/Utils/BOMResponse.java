package com.example.teamredrmit.weatherapp.Utils;

import com.example.teamredrmit.weatherapp.Model.BOMObservations;

public class BOMResponse {
    private BOMObservations observations;

    public BOMObservations getObservations() {
        return observations;
    }

    public void setObservations(BOMObservations o) {
        observations = o;
    }
}
