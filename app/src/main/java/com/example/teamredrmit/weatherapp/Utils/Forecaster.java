package com.example.teamredrmit.weatherapp.Utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.example.teamredrmit.weatherapp.Model.Observation;
import com.example.teamredrmit.weatherapp.R;

public class Forecaster extends ContextWrapper {
    public Forecaster(Context context) {
        super(context);
    }

    public Drawable getWeatherDrawable(String weather, String cloud, Float temperature) {
        int resource = getResourceId(weather, cloud, temperature);
        return ContextCompat.getDrawable(this, resource);
    }

    public Drawable getWeatherDrawable(String weather) {
        int resource = getResourceId(weather);
        return ContextCompat.getDrawable(this, resource);
    }

    // Quick function for mapping Forecast.io API icons to resources

    private int getResourceId(String weather) {
        switch (weather) {
            case "clear-day":
                return R.drawable.fine_mostlyclear;
            case "rain":
                return R.drawable.rain_cloudy;
            case "fog":
                return R.drawable.haze_cloudy;
            case "cloudy":
                return R.drawable.fine_cloudy;
            case "partly-cloudy-day":
                return R.drawable.fine_partlycloudy;
            default:
                return R.drawable.fine_mostlyclear;
        }
    }

    private int getResourceId(String weather,
                              String cloud, Float temperature) {

        if (temperature < Observation.Limits.lowerTemperatureLimit) {
            return R.drawable.cold_warning;
        } else if (temperature > Observation.Limits.upperTemperatureLimit) {
            return R.drawable.heat_warning;
        }

        switch (weather) {
            case "Fine":
                switch (cloud) {
                    case "Cloudy":
                        return R.drawable.fine_cloudy;
                    case "Partly cloudy":
                        return R.drawable.fine_partlycloudy;
                    default:
                        return R.drawable.fine_mostlyclear;
                }
            case "Showers":
                switch (cloud) {
                    case "Cloudy":
                        return R.drawable.showers_cloudy;
                    case "Partly cloudy":
                        return R.drawable.showers_partlycloudy;
                    default:
                        return R.drawable.showers_mostlyclear;
                }
            case "Rain":
                switch (cloud) {
                    case "Cloudy":
                        return R.drawable.rain_cloudy;
                    case "Partly cloudy":
                        return R.drawable.rain_partlycloudy;
                    default:
                        return R.drawable.rain_mostlyclear;
                }
            case "Drizzle":
                switch (cloud) {
                    case "Cloudy":
                        return R.drawable.drizzle_cloudy;
                    case "Partly cloudy":
                        return R.drawable.drizzle_partlycloudy;
                    default:
                        return R.drawable.drizzle_mostlyclear;
                }
            case "Haze":
                switch (cloud) {
                    case "Cloudy":
                        return R.drawable.haze_cloudy;
                    case "Partly cloudy":
                        return R.drawable.haze_partlycloudy;
                    default:
                        return R.drawable.haze_mostlyclear;
                }
            case "Mist":
                switch (cloud) {
                    case "Cloudy":
                        return R.drawable.mist_cloudy;
                    case "Partly cloudy":
                        return R.drawable.mist_partlycloudy;
                    default:
                        return R.drawable.mist_mostlyclear;
                }
            default:
                switch (cloud) {
                    case "Cloudy":
                        return R.drawable.fine_cloudy;
                    case "Partly cloudy":
                        return R.drawable.fine_partlycloudy;
                    default:
                        return R.drawable.fine_mostlyclear;
                }
        }
    }
}
