package com.example.teamredrmit.weatherapp.Utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import org.joda.time.DateTime;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

public class Query<T> implements Callable<T> {
    private static final String TAG = "Query";
    private URL path;
    private Class<T> type;

    public Query(Class<T> type, URL path) {
        this.path = path;
        this.type = type;
    }

    public T call() throws Exception {
        HttpURLConnection connection = (HttpURLConnection) path.openConnection();
        Log.i(TAG, "Connection established");
        try {
            InputStream response = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(response));

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(DateTime.class, new DateTimeDeserializer())
                    .create();

                   return gson.fromJson(new JsonReader(reader), type);

        }
        catch (Exception e){
            Log.e(TAG, e.getMessage());
        }
        finally {
            connection.disconnect();
        }
        return null;
    }
}
