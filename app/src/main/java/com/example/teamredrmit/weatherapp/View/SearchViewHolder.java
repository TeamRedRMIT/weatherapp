package com.example.teamredrmit.weatherapp.View;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.teamredrmit.weatherapp.R;


/**
 * Created by rudhrakumargurunathan on 13/04/16.
 */

public class SearchViewHolder extends RecyclerView.ViewHolder {

    private TextView cityText;
    private TextView stateText;
    private ImageButton favButton;

    public View searchView;

    public SearchViewHolder(View itemView)
    {
        super(itemView);
        searchView = itemView;

        cityText = (TextView)itemView.findViewById(R.id.tvSearchCityName);
        stateText = (TextView) itemView.findViewById(R.id.tvSearchStateName);
        favButton = (ImageButton) itemView.findViewById(R.id.btnFavourite);

    }
    public TextView getCityText(){
        return this.cityText;
    }

    public TextView getStateText(){
        return this.stateText;
    }

    public ImageButton getFavButton() {
        return this.favButton;
    }


}
