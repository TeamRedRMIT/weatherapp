package com.example.teamredrmit.weatherapp.View;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.teamredrmit.weatherapp.Controller.MainActivity;
import com.example.teamredrmit.weatherapp.Controller.WeatherService;
import com.example.teamredrmit.weatherapp.Model.ForecastQuery;
import com.example.teamredrmit.weatherapp.Model.Observation;
import com.example.teamredrmit.weatherapp.Model.ObservationListener;
import com.example.teamredrmit.weatherapp.Model.ObservationStore;
import com.example.teamredrmit.weatherapp.Model.Station;

import com.example.teamredrmit.weatherapp.R;
import com.example.teamredrmit.weatherapp.Utils.Forecaster;
import com.example.teamredrmit.weatherapp.View.Fragments.GraphFragment;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.ListenableFuture;

import java.net.URL;
import java.util.List;

public class WeatherTables extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "WeatherTables";

    public enum MeasurementType {
        TEMPERATURE,
        WIND,
        RAIN
    }

    private View child;

    private ObservationListener listener;
    private Optional<Observation> latest;

    public WeatherTables(){}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ObservationStore store = ObservationStore.getInstance();

        setContentView(R.layout.activity_weather_tables);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("station")) {
            Bundle b = intent.getExtras();
            Station s = b.getParcelable("station");
            GraphFragment graph = (GraphFragment) getFragmentManager()
                    .findFragmentById(R.id.graphFragment);
            if (graph != null) {
                graph.setStation(s);
                Optional<List<Observation>> observations = store.getAllObservations(s);
                if (observations.isPresent() ) {
                    List<Observation> readings = observations.get();
                    graph.setObservations(readings);

                    latest = Optional.fromNullable(
                            Iterables.getLast(readings, null));
                }
            }
        }

        listener = store.addObservationListener(new ObservationListener() {
            @Override
            public void onObservationAdded(Observation o, Station s) {
                latest = Optional.of(o);
            }
        });

        selectTable(MeasurementType.TEMPERATURE, 3);

        //Spinner code
        Spinner table_spinner = (Spinner) findViewById(R.id.table_spinner);
        if (table_spinner != null) {
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.table_options, R.layout.selected_spinner_table_item);
            adapter.setDropDownViewResource(R.layout.spinner_item);
            table_spinner.setAdapter(adapter);
            table_spinner.setOnItemSelectedListener(this);
        }

        Spinner graph_spinner = (Spinner) findViewById(R.id.graph_spinner);
        if (graph_spinner != null) {
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.graph_options, R.layout.selected_spinner_item);
            adapter.setDropDownViewResource(R.layout.spinner_graph_item);
            graph_spinner.setAdapter(adapter);
            graph_spinner.setOnItemSelectedListener(this);
        }

        if (latest.isPresent()) {
            WeatherService service = store.getService();
            Observation observation = latest.get();
            Optional<URL> query = ForecastQuery.getForecastURL(
                    observation.getLat(), observation.getLon());

            if (!query.isPresent())
                return;

            ListenableFuture forecast = service.getCurrentForecast(query.get());
            ViewStub stub = (ViewStub) findViewById(R.id.forecasts_stub);
            if (stub != null) {
                View inflated = stub.inflate();
                RecyclerView list = (RecyclerView) inflated.findViewById(R.id.forecastList);
                if (list != null) {
                    list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    list.setAdapter(new ForecastAdapter(new Forecaster(this), forecast));
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ObservationStore.getInstance().
                removeObservationListener(listener);
    }

    @Override
    public void onResume() {
        super.onResume();
        ObservationStore.getInstance().
                addObservationListener(listener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ObservationStore.getInstance().
                removeObservationListener(listener);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;

        if (spinner.getId() == R.id.table_spinner) {
            Spinner graph_spinner = (Spinner) findViewById(R.id.graph_spinner);
            int graphPosition = getSpinnerPosition(graph_spinner);
            int nodeInterval;

            switch (graphPosition) {
                case 0:
                    nodeInterval = 3;
                    break;
                case 1:
                    nodeInterval = 2;
                    break;
                default:
                    nodeInterval = 1;
            }

            switch (position) {
                case 0:
                    selectTable(MeasurementType.TEMPERATURE, nodeInterval);
                    Log.i(TAG, "User selected temperature table");
                    break;
                case 1:
                    selectTable(MeasurementType.WIND, nodeInterval);
                    Log.i(TAG, "User selected wind table");
                    break;
                default:
                    selectTable(MeasurementType.RAIN, nodeInterval);
                    Log.i(TAG, "User selected rain table");
            }

        } else if (spinner.getId() == R.id.graph_spinner) {
            Spinner table_spinner = (Spinner) findViewById(R.id.table_spinner);
            int tablePosition = getSpinnerPosition(table_spinner);
            MeasurementType type;

            switch (tablePosition) {
                case 0:
                    type = MeasurementType.TEMPERATURE;
                    break;
                case 1:
                    type = MeasurementType.WIND;
                    break;
                default:
                    type = MeasurementType.RAIN;
            }

            switch (position) {
                case 0:
                    selectTable(type, 3);
                    Log.i(TAG, "User selected 3 hour time intervals");
                    break;
                case 1:
                    selectTable(type, 2);
                    Log.i(TAG, "User selected 2 hour time intervals");
                    break;
                default:
                    selectTable(type, 1);
                    Log.i(TAG, "User selected 1 hour time interval");
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    //Selects which table to inflate based off spinner selection.
    public void selectTable(MeasurementType selection, int nodeNumber) {
        if (latest.isPresent()) {

            GraphFragment graph = (GraphFragment) getFragmentManager()
                    .findFragmentById(R.id.graphFragment);

            if (graph != null) {
                graph.toggleGraph(selection, nodeNumber);
            }

            displayResults(latest.get(), selection);
        }
    }

    private int getSpinnerPosition(Spinner spinner) {
        int measurement = spinner.getSelectedItemPosition();
        return measurement;
    }

    //Reloads home page when back pressed to refresh card views.
    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }

    public void displayResults(Observation latestObservation, MeasurementType selection) {
        LinearLayout item = (LinearLayout) findViewById(R.id.linear);
        item.removeAllViews();
        final int textViews = 5;
        final int textViewsInRainSpinner = 3;
        if (selection.equals(MeasurementType.TEMPERATURE)) {
            child = getLayoutInflater().inflate(R.layout.temperature_tables, null, false);

            TableLayout tl = (TableLayout) child.findViewById(R.id.temp_table);
            TableRow tr = new TableRow(this);

            for (int i = 0; i < textViews; i++) {
                TextView tv = new TextView(this);
                switch (i) {
                    case 0:
                        tv.setText(getFormattedStringForFloat(latestObservation.getAirTemp()));
                        break;
                    case 1:
                        tv.setText(getFormattedStringForDouble(latestObservation.getApparentT()));
                        break;
                    case 2:
                        tv.setText(getFormattedStringForDouble(latestObservation.getDewpt()));
                        break;
                    case 3:
                        tv.setText(getFormattedStringForInt(latestObservation.getRelHum()));
                        break;
                    case 4:
                        tv.setText(getFormattedStringForDouble(latestObservation.getDeltaT()));
                        break;
                    default:
                        tv.setText("-");
                }
                tv.setLayoutParams(new TableRow.LayoutParams(
                        TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT, 1f));
                tv.setGravity(Gravity.CENTER);
                tr.setPadding(20,0,20,0);
                tr.addView(tv);
            }
            tl.addView(tr);

        } else if (selection.equals(MeasurementType.WIND)) {
            child = getLayoutInflater().inflate(R.layout.wind_tables, null, false);

            TableLayout tl = (TableLayout) child.findViewById(R.id.wind_table);
            TableRow tr = new TableRow(this);

            for (int i = 0; i < textViews; i++) {
                TextView tv = new TextView(this);
                switch (i) {
                    case 0:
                        tv.setText(getFormattedString(latestObservation.getWindDir()));
                        break;
                    case 1:
                        tv.setText(getFormattedStringForInt(latestObservation.getWindSpdKmh()));
                        break;
                    case 2:
                        tv.setText(getFormattedStringForObject(latestObservation.getGustKmh()));
                        break;
                    case 3:
                        tv.setText(getFormattedStringForInt(latestObservation.getWindSpdKt()));
                        break;
                    case 4:
                        tv.setText(getFormattedStringForObject(latestObservation.getGustKt()));
                        break;
                    default:
                        tv.setText("-");
                }
                tv.setLayoutParams(new TableRow.LayoutParams(
                        TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT, 1f));
                tv.setGravity(Gravity.CENTER);
                tr.addView(tv);
            }
            tl.addView(tr);

        } else if (selection.equals(MeasurementType.RAIN)) {
            child = getLayoutInflater().inflate(R.layout.pressure_rain_tables, null, false);

            TableLayout tl = (TableLayout) child.findViewById(R.id.press_rain_table);
            TableRow tr = new TableRow(this);

            for (int i = 0; i < textViewsInRainSpinner; i++) {
                TextView tv = new TextView(this);
                switch (i) {
                    case 0:
                        tv.setText(getFormattedStringForDouble(latestObservation.getPressQnh()));
                        break;
                    case 1:
                        tv.setText(getFormattedStringForDouble(latestObservation.getPressMsl()));
                        break;
                    case 2:
                        tv.setText(getFormattedString(latestObservation.getRainTrace()));
                        break;
                    default:
                        tv.setText("-");
                }
                tv.setLayoutParams(new TableRow.LayoutParams(
                        TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT, 1f));
                tv.setGravity(Gravity.CENTER);
                tr.addView(tv);
            }
            tl.addView(tr);
        } else {
            TextView tv = new TextView(this);
            tv.setText("Error retrieving table data.");
        }
        item.addView(child);

    }

    private String getFormattedStringForDouble(Double input) {
        if (input != null) {
            return String.format("%.1f", input);
        } else {
            return "_";
        }
    }

    private String getFormattedStringForInt(Integer input) {
        if (input != null) {
            return input.toString();
        } else {
            return "_";
        }
    }

    private String getFormattedStringForObject(Object input) {
        if (input != null) {
            return input.toString();
        } else {
            return "_";
        }
    }

    private String getFormattedString(String input) {

        if (input != null) {
            return input.toString();
        } else {
            return "_";
        }

    }

    private String getFormattedStringForFloat(Float input) {
        if (input != null) {
            return String.format("%.1f", input);
        } else {
            return "_";
        }
    }

}
