package com.example.teamredrmit.weatherapp.Controller;

import com.example.teamredrmit.weatherapp.Model.Observation;
import com.google.common.collect.Ordering;
import org.joda.time.DateTimeComparator;

public class ObservationOrdering extends Ordering<Observation> {
    @Override
    public int compare(Observation o1, Observation o2) {
        DateTimeComparator comparator = DateTimeComparator.getInstance();
        return comparator.compare(o1.getLocalDateTimeFull(), o2.getLocalDateTimeFull());
    }
}