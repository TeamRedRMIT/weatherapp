package com.example.teamredrmit.weatherapp.Controller;

import android.content.SharedPreferences;

import com.example.teamredrmit.weatherapp.Model.ObservationStore;
import com.example.teamredrmit.weatherapp.Model.Station;
import com.example.teamredrmit.weatherapp.Model.StationListener;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Favourites {
    private static final String key = "stations";

    private List<StationListener> listeners;
    private List<Station> stations;

    private SharedPreferences preferences;
    private ObservationStore provider;
    private Firebase firebase;

    public Favourites(Firebase fb, ObservationStore store,
                      SharedPreferences s) {
        firebase = fb;
        provider = store;
        preferences = s;

        listeners = new ArrayList<>();
        stations = new ArrayList<>();

        loadSavedStations();
    }

    public void removeStation(Station station) {

        if (stations.contains(station)) {
            provider.removeStation(station);
            stations.remove(station);

        }

        Set<String> keys = preferences.getStringSet(key, new HashSet<String>());
        if (keys.contains(station.getId())) {

            // A new instance has to be created, otherwise the changes
            // are not reflected back to SharedPreferences correctly
            Set<String> updatedKeys = new HashSet<>(keys);
            updatedKeys.remove(station.getId());

            // Commit the updated String Set back to SharedPreferences
            SharedPreferences.Editor e = preferences.edit();
            e.putStringSet(key, updatedKeys);
            e.apply();
        }
    }

    public void addStation(Station s) {
        Set<String> keys = preferences.getStringSet(key, new HashSet<String>());

        if (!keys.contains(s.getId())) {
            Set<String> updatedKeys = new HashSet<>(keys);
            updatedKeys.add(s.getId());

            SharedPreferences.Editor e = preferences.edit();
            e.putStringSet(key, updatedKeys);
            e.apply();

            stations.add(s);
            provider.observeStation(s);
            notifyAllStationListeners(s);
        }
    }

    public StationListener addStationListener(StationListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }

        return listener;
    }

    public void removeStationListener(StationListener listener) {
        listeners.remove(listener);
    }

    private void loadSavedStations() {
        Set<String> ids = preferences.getStringSet(key, new HashSet<String>());
        for (String id : ids) {
            loadStationById(id);
        }
    }

    private void loadStationById(String id) {
        Integer i = Integer.valueOf(id);
        firebase.child("stations").orderByChild("id").equalTo(i)
                .addChildEventListener(firebaseListener);
    }

    private void notifyAllStationListeners(Station s) {
        for (StationListener listener : listeners) {
            listener.onStationAdded(s);
        }
    }

    private ChildEventListener firebaseListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Station row = dataSnapshot.getValue(Station.class);
            if (!stations.contains(row)) {
                stations.add(row);
                provider.observeStation(row);
                notifyAllStationListeners(row);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    };

    public void onDestroy() {
        firebase.removeEventListener(firebaseListener);
        listeners.clear();
    }
}
