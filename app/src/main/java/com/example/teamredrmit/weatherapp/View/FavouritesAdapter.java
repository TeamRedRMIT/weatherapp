package com.example.teamredrmit.weatherapp.View;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.teamredrmit.weatherapp.Controller.Favourites;
import com.example.teamredrmit.weatherapp.Model.Observation;
import com.example.teamredrmit.weatherapp.Model.ObservationListener;
import com.example.teamredrmit.weatherapp.Model.ObservationStore;
import com.example.teamredrmit.weatherapp.Model.Station;
import com.example.teamredrmit.weatherapp.Model.StationListener;
import com.example.teamredrmit.weatherapp.R;
import com.example.teamredrmit.weatherapp.Utils.Forecaster;
import com.google.common.base.Optional;

import java.util.ArrayList;
import java.util.List;

public class FavouritesAdapter
        extends RecyclerView.Adapter<StationViewHolder> {

    private List<Station> stations;
    private ObservationStore measurements;
    private Forecaster forecaster;

    private ObservationListener oListener;
    private StationListener sListener;
    private Favourites favourites;


    // This is required so that observation events notify the adapter of
    // item changes in the UI thread after the RecyclerView is ready
    private Handler handler = new Handler();
    private ItemTouchHelper touchHelper;

    public FavouritesAdapter(final Favourites provider,
                             ObservationStore store, Forecaster f, RecyclerView rv) {
        forecaster = f;
        measurements = store;
        favourites = provider;
        stations = new ArrayList<>();

        oListener = store.addObservationListener(new ObservationListener() {
            @Override
            public void onObservationAdded(final Observation o, final Station s) {
                handler.post(new Runnable() {
                    public void run() {
                        int i = stations.indexOf(s);
                        notifyItemChanged(i);
                    }
                });
            }
        });

        sListener = provider.addStationListener(new StationListener() {
            @Override
            public void onStationAdded(final Station s) {
                if (!stations.contains(s)) {
                    stations.add(s);
                    notifyDataSetChanged();
                }
            }
        });

        ItemTouchHelper.SimpleCallback touchListener = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDirection) {
                int i = viewHolder.getAdapterPosition();
                Station station = stations.get(i);
                stations.remove(i);
                provider.removeStation(station);
                notifyDataSetChanged();
            }
        };

        touchHelper = new ItemTouchHelper(touchListener);
        touchHelper.attachToRecyclerView(rv);
    }

    @Override
    public void onBindViewHolder(StationViewHolder stationViewHolder, int i) {
        final Station row = stations.get(i);
        stationViewHolder.cardContainer.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent i = new Intent(forecaster.getApplicationContext(), WeatherTables.class);
                i.putExtra("station", row);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                forecaster.getApplicationContext().startActivity(i);
            }
        });

        // Set name of the station in the view
        stationViewHolder.name.setText(row.getName());

        // Get the latest Observation for the current station from the ObservationStore
        Optional<Observation> o = measurements.getLastObservation(row);
        if (o.isPresent()) {
            Observation current = o.get();
            // Set the current temperature text field
            Float temperature = current.getAirTemp();
            if (temperature == null) {
                temperature = 0.0f;
            }

            stationViewHolder.currentTemp.setText(String.format("%.1f°", temperature));

            if (temperature < Observation.Limits.lowerTemperatureLimit) {
                stationViewHolder.cardContainer.setBackgroundColor(Color.parseColor("#53B8FC"));
                stationViewHolder.name.setBackgroundColor(Color.parseColor("#53B8FC"));
                stationViewHolder.name.setTextColor(Color.WHITE);
                stationViewHolder.currentTemp.setTextColor(Color.WHITE);

            } else if (temperature > Observation.Limits.upperTemperatureLimit) {
                stationViewHolder.cardContainer.setBackgroundColor(Color.parseColor("#E74C3C"));
                stationViewHolder.name.setBackgroundColor(Color.parseColor("#E74C3C"));
                stationViewHolder.name.setTextColor(Color.WHITE);
                stationViewHolder.currentTemp.setTextColor(Color.WHITE);
            } else {
                stationViewHolder.cardContainer.setBackgroundColor(Color.WHITE);
                stationViewHolder.name.setBackgroundColor(Color.WHITE);
                stationViewHolder.name.setTextColor(Color.parseColor("#313A46"));
                stationViewHolder.currentTemp.setTextColor(Color.parseColor("#BEC3C7"));

            }

            Drawable icon = forecaster.getWeatherDrawable(current.getWeather(), current.getCloud(), temperature);
            stationViewHolder.weatherImage.setImageDrawable(icon);

        } else {
            stationViewHolder.currentTemp.setText(String.format("%.1f°", 0.0));
        }
    }

    @Override
    public StationViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cards, viewGroup, false);
        return new StationViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    public void onStop() {
        measurements.removeObservationListener(oListener);
        favourites.removeStationListener(sListener);
    }

    public void onResume() {
        favourites.addStationListener(sListener);
        measurements.addObservationListener(oListener);
    }
}
