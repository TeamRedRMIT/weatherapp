package com.example.teamredrmit.weatherapp.Utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rudhrakumargurunathan on 28/05/16.
 */
public class Validator {
    //Return Integer Formatted Temp values
    public static Double round(Double input){
        Log.i("Validator",input.toString());
        double result = 0;
        if(input < 0){
            if((input - Math.floor(input)) < 0.5){
                result  = Math.floor(input);
            }
            else{
                result  = Math.ceil(input);
            }
        }
        else if (input > 0){
            if((input - Math.floor(input)) > 0.5){
                result  = Math.ceil(input);
            }
            else{
                result  = Math.floor(input);
            }
        }
        System.out.print(result);
        return result;
    }

    public static String validateSearchInput(String inputText){
        String result = "";
        if(inputText.isEmpty() || inputText.trim().isEmpty()){
            result = "isEmpty";
        }
        else if(!(inputText.matches("^[a-zA-Z\\s]+$"))){ // alphabets & white space allowed
            result = "InvalidInput";
        }
        else{
            result = "ValidInput";
        }
        return result;
    }

    public static String getWeekDay(int timeStamp){
        Date dateTime = new Date((long)timeStamp*1000);
        String weekDay = new SimpleDateFormat("EEE").format(dateTime);
        return weekDay;
    }

    public static String getTime(int timeStamp){
        Date dateTime = new Date((long)timeStamp*1000);

        String timeString = new SimpleDateFormat("hh:mm").format(dateTime);

        return timeString;
    }
}
