package com.example.teamredrmit.weatherapp.Utils;

public class Coordinate {

    public final Double longitude;
    public final Double latitude;

    public Coordinate(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
