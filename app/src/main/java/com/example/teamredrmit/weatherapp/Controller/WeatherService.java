package com.example.teamredrmit.weatherapp.Controller;

import com.example.teamredrmit.weatherapp.Model.Forecast;
import com.example.teamredrmit.weatherapp.Utils.BOMResponse;
import com.example.teamredrmit.weatherapp.Utils.Query;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.net.URL;
import java.util.concurrent.ExecutorService;

public class WeatherService {
    private ListeningExecutorService pool;

    public WeatherService(ExecutorService executor) {
        pool = MoreExecutors.listeningDecorator(executor);
    }

    public ListenableFuture<BOMResponse> getCurrentMeasurements(URL path) {
        return pool.submit(new Query<>(BOMResponse.class, path));
    }

    public ListenableFuture<Forecast> getCurrentForecast(URL path) {
        return pool.submit(new Query<>(Forecast.class, path));
    }
}