package com.example.teamredrmit.weatherapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.Objects;

public class Observation {

    public static class Limits {
        public static final Float upperTemperatureLimit = 35.0f;
        public static final Float lowerTemperatureLimit = 5.0f;
    }

    @SerializedName("sort_order")
    @Expose
    private Integer sortOrder;
    @SerializedName("wmo")
    @Expose
    private Integer wmo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("history_product")
    @Expose
    private String historyProduct;
    @SerializedName("local_date_time")
    @Expose
    private String localDateTime;
    @SerializedName("local_date_time_full")
    @Expose
    private DateTime localDateTimeFull;
    @SerializedName("aifstime_utc")
    @Expose
    private String aifstimeUtc;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("apparent_t")
    @Expose
    private Double apparentT;
    @SerializedName("cloud")
    @Expose
    private String cloud;
    @SerializedName("cloud_base_m")
    @Expose
    private Object cloudBaseM;
    @SerializedName("cloud_oktas")
    @Expose
    private Object cloudOktas;
    @SerializedName("cloud_type_id")
    @Expose
    private Object cloudTypeId;
    @SerializedName("cloud_type")
    @Expose
    private String cloudType;
    @SerializedName("delta_t")
    @Expose
    private Double deltaT;
    @SerializedName("gust_kmh")
    @Expose
    private Object gustKmh;
    @SerializedName("gust_kt")
    @Expose
    private Object gustKt;
    @SerializedName("air_temp")
    @Expose
    private Float airTemp;
    @SerializedName("dewpt")
    @Expose
    private Double dewpt;
    @SerializedName("press")
    @Expose
    private Double press;
    @SerializedName("press_qnh")
    @Expose
    private Double pressQnh;
    @SerializedName("press_msl")
    @Expose
    private Double pressMsl;
    @SerializedName("press_tend")
    @Expose
    private String pressTend;
    @SerializedName("rain_trace")
    @Expose
    private String rainTrace;
    @SerializedName("rel_hum")
    @Expose
    private Integer relHum;
    @SerializedName("sea_state")
    @Expose
    private String seaState;
    @SerializedName("swell_dir_worded")
    @Expose
    private String swellDirWorded;
    @SerializedName("swell_height")
    @Expose
    private Object swellHeight;
    @SerializedName("swell_period")
    @Expose
    private Object swellPeriod;
    @SerializedName("vis_km")
    @Expose
    private String visKm;
    @SerializedName("weather")
    @Expose
    private String weather;
    @SerializedName("wind_dir")
    @Expose
    private String windDir;
    @SerializedName("wind_spd_kmh")
    @Expose
    private Integer windSpdKmh;
    @SerializedName("wind_spd_kt")
    @Expose
    private Integer windSpdKt;

    public Observation(Integer sortOrder, Integer wmo, String name, String historyProduct,
                       String localDateTime, DateTime localDateTimeFull, String aifstimeUtc,
                       Double lat, Double lon, Double apparentT, String cloud, Object cloudBaseM,
                       Object cloudOktas, Object cloudTypeId, String cloudType, Double deltaT,
                       Object gustKmh, Object gustKt, Float airTemp, Double dewpt, Double press,
                       Double pressQnh, Double pressMsl, String pressTend, String rainTrace,
                       Integer relHum, String seaState, String swellDirWorded, Object swellHeight,
                       Object swellPeriod, String visKm, String weather, String windDir,
                       Integer windSpdKmh, Integer windSpdKt) {
        this.sortOrder = sortOrder;
        this.wmo = wmo;
        this.name = name;
        this.historyProduct = historyProduct;
        this.localDateTime = localDateTime;
        this.localDateTimeFull = localDateTimeFull;
        this.aifstimeUtc = aifstimeUtc;
        this.lat = lat;
        this.lon = lon;
        this.apparentT = apparentT;
        this.cloud = cloud;
        this.cloudBaseM = cloudBaseM;
        this.cloudOktas = cloudOktas;
        this.cloudTypeId = cloudTypeId;
        this.cloudType = cloudType;
        this.deltaT = deltaT;
        this.gustKmh = gustKmh;
        this.gustKt = gustKt;
        this.airTemp = airTemp;
        this.dewpt = dewpt;
        this.press = press;
        this.pressQnh = pressQnh;
        this.pressMsl = pressMsl;
        this.pressTend = pressTend;
        this.rainTrace = rainTrace;
        this.relHum = relHum;
        this.seaState = seaState;
        this.swellDirWorded = swellDirWorded;
        this.swellHeight = swellHeight;
        this.swellPeriod = swellPeriod;
        this.visKm = visKm;
        this.weather = weather;
        this.windDir = windDir;
        this.windSpdKmh = windSpdKmh;
        this.windSpdKt = windSpdKt;
    }

    /**
     * @return The sortOrder
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * @param sortOrder The sort_order
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * @return The wmo
     */
    public Integer getWmo() {
        return wmo;
    }

    /**
     * @param wmo The wmo
     */
    public void setWmo(Integer wmo) {
        this.wmo = wmo;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The historyProduct
     */
    public String getHistoryProduct() {
        return historyProduct;
    }

    /**
     * @param historyProduct The history_product
     */
    public void setHistoryProduct(String historyProduct) {
        this.historyProduct = historyProduct;
    }

    /**
     * @return The localDateTime
     */
    public String getLocalDateTime() {
        return localDateTime;
    }

    /**
     * @param localDateTime The local_date_time
     */
    public void setLocalDateTime(String localDateTime) {
        this.localDateTime = localDateTime;
    }

    /**
     * @return The localDateTimeFull
     */
    public DateTime getLocalDateTimeFull() {
        return localDateTimeFull;
    }

    /**
     * @param localDateTimeFull The local_date_time_full
     */
    public void setLocalDateTimeFull(DateTime localDateTimeFull) {
        this.localDateTimeFull = localDateTimeFull;
    }

    /**
     * @return The aifstimeUtc
     */
    public String getAifstimeUtc() {
        return aifstimeUtc;
    }

    /**
     * @param aifstimeUtc The aifstime_utc
     */
    public void setAifstimeUtc(String aifstimeUtc) {
        this.aifstimeUtc = aifstimeUtc;
    }

    /**
     * @return The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     * @return The apparentT
     */
    public Double getApparentT() {
        return apparentT;
    }

    /**
     * @param apparentT The apparent_t
     */
    public void setApparentT(Double apparentT) {
        this.apparentT = apparentT;
    }

    /**
     * @return The cloud
     */
    public String getCloud() {
        return cloud;
    }

    /**
     * @param cloud The cloud
     */
    public void setCloud(String cloud) {
        this.cloud = cloud;
    }

    /**
     * @return The cloudBaseM
     */
    public Object getCloudBaseM() {
        return cloudBaseM;
    }

    /**
     * @param cloudBaseM The cloud_base_m
     */
    public void setCloudBaseM(Object cloudBaseM) {
        this.cloudBaseM = cloudBaseM;
    }

    /**
     * @return The cloudOktas
     */
    public Object getCloudOktas() {
        return cloudOktas;
    }

    /**
     * @param cloudOktas The cloud_oktas
     */
    public void setCloudOktas(Object cloudOktas) {
        this.cloudOktas = cloudOktas;
    }

    /**
     * @return The cloudTypeId
     */
    public Object getCloudTypeId() {
        return cloudTypeId;
    }

    /**
     * @param cloudTypeId The cloud_type_id
     */
    public void setCloudTypeId(Object cloudTypeId) {
        this.cloudTypeId = cloudTypeId;
    }

    /**
     * @return The cloudType
     */
    public String getCloudType() {
        return cloudType;
    }

    /**
     * @param cloudType The cloud_type
     */
    public void setCloudType(String cloudType) {
        this.cloudType = cloudType;
    }

    /**
     * @return The deltaT
     */
    public Double getDeltaT() {
        return deltaT;
    }

    /**
     * @param deltaT The delta_t
     */
    public void setDeltaT(Double deltaT) {
        this.deltaT = deltaT;
    }

    /**
     * @return The gustKmh
     */
    public Object getGustKmh() {
        return gustKmh;
    }

    /**
     * @param gustKmh The gust_kmh
     */
    public void setGustKmh(Object gustKmh) {
        this.gustKmh = gustKmh;
    }

    /**
     * @return The gustKt
     */
    public Object getGustKt() {
        return gustKt;
    }

    /**
     * @param gustKt The gust_kt
     */
    public void setGustKt(Object gustKt) {
        this.gustKt = gustKt;
    }

    /**
     * @return The airTemp
     */
    public Float getAirTemp() {
        return airTemp;
    }

    /**
     * @param airTemp The air_temp
     */
    public void setAirTemp(Float airTemp) {
        this.airTemp = airTemp;
    }

    /**
     * @return The dewpt
     */
    public Double getDewpt() {
        return dewpt;
    }

    /**
     * @param dewpt The dewpt
     */
    public void setDewpt(Double dewpt) {
        this.dewpt = dewpt;
    }

    /**
     * @return The press
     */
    public Double getPress() {
        return press;
    }

    /**
     * @param press The press
     */
    public void setPress(Double press) {
        this.press = press;
    }

    /**
     * @return The pressQnh
     */
    public Double getPressQnh() {
        return pressQnh;
    }

    /**
     * @param pressQnh The press_qnh
     */
    public void setPressQnh(Double pressQnh) {
        this.pressQnh = pressQnh;
    }

    /**
     * @return The pressMsl
     */
    public Double getPressMsl() {
        return pressMsl;
    }

    /**
     * @param pressMsl The press_msl
     */
    public void setPressMsl(Double pressMsl) {
        this.pressMsl = pressMsl;
    }

    /**
     * @return The pressTend
     */
    public String getPressTend() {
        return pressTend;
    }

    /**
     * @param pressTend The press_tend
     */
    public void setPressTend(String pressTend) {
        this.pressTend = pressTend;
    }

    /**
     * @return The rainTrace
     */
    public String getRainTrace() {
        return rainTrace;
    }

    /**
     * @param rainTrace The rain_trace
     */
    public void setRainTrace(String rainTrace) {
        this.rainTrace = rainTrace;
    }

    /**
     * @return The relHum
     */
    public Integer getRelHum() {
        return relHum;
    }

    /**
     * @param relHum The rel_hum
     */
    public void setRelHum(Integer relHum) {
        this.relHum = relHum;
    }

    /**
     * @return The seaState
     */
    public String getSeaState() {
        return seaState;
    }

    /**
     * @param seaState The sea_state
     */
    public void setSeaState(String seaState) {
        this.seaState = seaState;
    }

    /**
     * @return The swellDirWorded
     */
    public String getSwellDirWorded() {
        return swellDirWorded;
    }

    /**
     * @param swellDirWorded The swell_dir_worded
     */
    public void setSwellDirWorded(String swellDirWorded) {
        this.swellDirWorded = swellDirWorded;
    }

    /**
     * @return The swellHeight
     */
    public Object getSwellHeight() {
        return swellHeight;
    }

    /**
     * @param swellHeight The swell_height
     */
    public void setSwellHeight(Object swellHeight) {
        this.swellHeight = swellHeight;
    }

    /**
     * @return The swellPeriod
     */
    public Object getSwellPeriod() {
        return swellPeriod;
    }

    /**
     * @param swellPeriod The swell_period
     */
    public void setSwellPeriod(Object swellPeriod) {
        this.swellPeriod = swellPeriod;
    }

    /**
     * @return The visKm
     */
    public String getVisKm() {
        return visKm;
    }

    /**
     * @param visKm The vis_km
     */
    public void setVisKm(String visKm) {
        this.visKm = visKm;
    }

    /**
     * @return The weather
     */
    public String getWeather() {
        return weather;
    }

    /**
     * @param weather The weather
     */
    public void setWeather(String weather) {
        this.weather = weather;
    }

    /**
     * @return The windDir
     */
    public String getWindDir() {
        return windDir;
    }

    /**
     * @param windDir The wind_dir
     */
    public void setWindDir(String windDir) {
        this.windDir = windDir;
    }

    /**
     * @return The windSpdKmh
     */
    public Integer getWindSpdKmh() {
        return windSpdKmh;
    }

    /**
     * @param windSpdKmh The wind_spd_kmh
     */
    public void setWindSpdKmh(Integer windSpdKmh) {
        this.windSpdKmh = windSpdKmh;
    }

    /**
     * @return The windSpdKt
     */
    public Integer getWindSpdKt() {
        return windSpdKt;
    }

    /**
     * @param windSpdKt The wind_spd_kt
     */
    public void setWindSpdKt(Integer windSpdKt) {
        this.windSpdKt = windSpdKt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Observation that = (Observation) o;
        return Objects.equals(localDateTimeFull, that.localDateTimeFull) &&
                Objects.equals(aifstimeUtc, that.aifstimeUtc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(localDateTimeFull, aifstimeUtc);
    }
}