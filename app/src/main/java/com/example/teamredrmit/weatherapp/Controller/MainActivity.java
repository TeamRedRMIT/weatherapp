package com.example.teamredrmit.weatherapp.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teamredrmit.weatherapp.Model.ObservationStore;
import com.example.teamredrmit.weatherapp.Model.Station;
import com.example.teamredrmit.weatherapp.R;
import com.example.teamredrmit.weatherapp.Utils.Forecaster;
import com.example.teamredrmit.weatherapp.Utils.Validator;
import com.example.teamredrmit.weatherapp.View.FavouritesAdapter;
import com.firebase.client.Firebase;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class MainActivity extends AppCompatActivity {

    private MenuItem searchAction;
    private EditText searchText;

    private boolean exit = false;
    private boolean isSearchOpened = false;

    // Need to keep track of the adapter to add and remove
    // the listeners when the Activity sleeps / an intent occurs
    private FavouritesAdapter adapter;
    private Favourites favourites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences preferences = getSharedPreferences("Weather", 0);


        // Retrieve the Singleton instance of the store that maintains the Observation states
        // for all the tracked stations.
        ObservationStore store = ObservationStore.getInstance();

        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Toolbar & Drawer code
        Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Firebase.setAndroidContext(getApplicationContext());
        Firebase firebase = new Firebase("https://crackling-fire-9834.firebaseIO.com");

        // Initialise favourites store
        favourites = new Favourites(firebase, store, preferences);

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        adapter = new FavouritesAdapter(favourites, store,
                new Forecaster(getApplicationContext()), rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        updateRefreshTime();

        // Handle the intent from search to add the required station
        // to favourites
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("station")) {
            Bundle b = intent.getExtras();
            Station s = b.getParcelable("station");
            favourites.addStation(s);
        }
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);

        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        searchAction = menu.findItem(R.id.actionbar_search);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case  R.id.actionbar_help:
                goToUrl("http://teamredrmit.bitbucket.org");
                return true;
            case R.id.actionbar_refresh:
                refreshMeasurements();
                return true;
            case R.id.actionbar_search:
                handleMenuSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.onStop();
        favourites.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.onResume();
    }

    public void handleMenuSearch(){
        android.support.v7.app.ActionBar action = getSupportActionBar(); //get the actionbar

        if (isSearchOpened) {

            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar

            //hides the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);

            //add the search icon in the action bar
            searchAction.setIcon(getResources().getDrawable(R.drawable.ic_search));

            isSearchOpened = false;
        } else { //open the search entry

            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.searchbar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title

            searchText = (EditText)action.getCustomView().findViewById(R.id.searchBarText); //the text editor

            //this is a listener to do a search when the user clicks on search button
            searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        String userInput = v.getText().toString();

                        doSearch(userInput);
                        //validateSearchInput(userInput);
                        searchText.setText("");
                        searchText.clearFocus();
                        //add the search icon in the action bar
                        searchAction.setIcon(getResources().getDrawable(R.drawable.ic_search));
                        isSearchOpened = false;
                        return true;
                    }
                    return false;
                }
            });

            searchText.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);

            //add the close icon
            searchAction.setIcon(getResources().getDrawable(R.drawable.ic_close));
            isSearchOpened = true;

        }
    }
    /*
    private void validateSearchInput(String inputText){
        if(inputText.isEmpty()){
            Toast.makeText(this, "Enter station name",
                    Toast.LENGTH_LONG).show();
        }
        else if(!(inputText.matches("^[a-zA-Z\\s]+$"))){ // alphabets & white space allowed
            Toast.makeText(getApplicationContext(), "Invalid Name Input",
                    Toast.LENGTH_LONG).show();
        }
        else{
            doSearch(inputText);
        }
    }
    */
    private void doSearch(String inputText) {
        String validationResult = Validator.validateSearchInput(inputText);
        if(validationResult.equals("isEmpty")){
            Toast.makeText(this, "Enter station name",
                    Toast.LENGTH_LONG).show();
        }
        else if(validationResult.equals("InvalidInput")){
            Toast.makeText(getApplicationContext(), "Invalid Name Input",
                    Toast.LENGTH_LONG).show();
        }
        else{
            //Reset the search bar and call search Intent
            android.support.v7.app.ActionBar action = getSupportActionBar();
            isSearchOpened = false;
            action.setDisplayShowCustomEnabled(false);
            action.setDisplayShowTitleEnabled(true);

            Intent searchIntent = new Intent(this, SearchActivity.class);
            searchIntent.putExtra("inputText", inputText);
            searchIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(searchIntent);
        }
    }

    public void refreshMeasurements() {
        ObservationStore.getInstance().updateAllObservations();
        updateRefreshTime();
    }

    public void updateRefreshTime() {
        DateTimeFormatter format = DateTimeFormat
                .forPattern("h:mma");
        LocalTime time = new LocalTime();
        String last_update = "Checked for updates at " + format.print(time);

        TextView tv_update = (TextView) findViewById(R.id.lastRefreshText);
        tv_update.setText(last_update);
    }

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        launchBrowser.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(launchBrowser);
        
    }

}