package com.example.teamredrmit.weatherapp.View;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.teamredrmit.weatherapp.R;

import java.util.Locale;

class ForecastHolder extends RecyclerView.ViewHolder {

    private TextView datetime;
    private TextView maxTemp;
    private TextView minTemp;
    private ImageView weatherImage;

    public ForecastHolder(View view) {
        super(view);

        datetime = (TextView) view.findViewById(R.id.forecast_datetime);
        maxTemp  = (TextView) view.findViewById(R.id.forecast_maxTemp);
        minTemp  = (TextView) view.findViewById(R.id.forecast_minTemp);
        weatherImage = (ImageView) view.findViewById(R.id.forecast_image);

    }

    public void setDatetime(String dt) {
        if (datetime != null) {
            datetime.setText(dt);
        }
    }

    public void setMaxTemp(Double t) {
        if (maxTemp != null) {
            maxTemp.setText(String.format(Locale.getDefault(), "%.1f°", t));
        }
    }

    public void setMinTemp(Double t) {
        if (minTemp != null) {
            minTemp.setText(String.format(Locale.getDefault(), "%.1f°", t));
        }
    }

    public void setWeatherImage(Drawable d) {
        weatherImage.setImageDrawable(d);
    }
}